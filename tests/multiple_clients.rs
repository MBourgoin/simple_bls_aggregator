//  (c) 2024 Mathias Bourgoin <mathias.bourgoin@gmail.com>
// SPDX-License-Identifier: BSD-3-Clause

use assert_cmd::prelude::*;
use std::process::Command;

//Run a server and 20 clients and check that they all finish successfully after
//10 levels
#[test]
fn run_all() {
    let clients = 20;
    let mut server_cmd = Command::cargo_bin("server").unwrap();

    let mut server_process = server_cmd
        .arg("--connections")
        .arg(clients.to_string())
        .arg("--max-level")
        .arg("10")
        .spawn()
        .unwrap();

    let mut client_processes = (0..clients)
        .map(|_| {
            let mut client = Command::cargo_bin("client").unwrap();
            client.spawn().unwrap()
        })
        .collect::<Vec<_>>();

    server_process
        .wait()
        .expect("Server process should finish successfully");

    for client in client_processes.iter_mut() {
        client
            .wait()
            .expect("Client process should finish successfully");
    }
}
