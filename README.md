# BLS Aggregator

Welcome to the BLS Aggregator, a playground project designed for experimenting with Rust crates and delving into signature aggregation. This project offers a simplistic Remote BLS-Signature aggregator, featuring a [basic server](src/bin/server.rs) for aggregating signatures from [straightforward clients](src/bin/client.rs).

## Overview

The network functionality is managed by [message-io](https://github.com/lemunozm/message-io), while BLS signatures are efficiently handled through [blst](https://github.com/supranational/blst). Additionally, the project employs [assert_cmd](https://github.com/assert-rs/assert_cmd) for comprehensive integration tests, exemplified in [multiple_clients.rs](tests/multiple_clients.rs).


### Importance of Signature Aggregation

Signature aggregation plays a crucial role in decentralized distributed systems, consensus algorithms, secure voting, and blockchains. It serves as a fundamental building block for scalability, reducing the size of on-chain data and minimizing the number of exchanged messages between nodes. For instance, in blockchains, it can condense multiple transaction signatures into a single signature per block, while in consensus operations, it optimizes message exchange, enhancing system performance, scalability, and cost-effectiveness.

While this project is a basic implementation, it provides a foundation for exploring more complex scenarios and applications.

## Usage

To initiate the server, execute:
```bash
RUST_LOG=info cargo run --bin server -- --max-level 100 --connections 10
```
Here, `max-level` dictates the number of aggregations the server performs before termination, and `connections` sets the number of anticipated clients.

To commence a client, utilize:
```bash
RUST_LOG=info cargo run --bin client
```

The server operates on port 7965, while client connections are presently hardcoded for local interaction.

### Server

1. Upon initialization, the server awaits the specified number of connections.
2. It then dispatches a 'Genesis' **Block** (level 0) message to all connected peers.
3. Subsequently, the server anticipates **Attestations** from clients at each `level`, signing the message and forwarding it to the server for aggregation.
4. A `level` concludes upon receiving an attestation from every client, with no expectation for duplicate client submissions.
5. Upon a `level`'s completion, the server aggregates all attestation signatures, forges a new block for the next level, including the aggregated signature,
 and broadcasts it to all connected clients.
6. The server stops after reaching the maximum level and thus having performed `max-level` signature aggregations.


### Client

1. The client generates a random BLS keypair and forwards the public key to the server.
2. It enters a loop, awaiting messages from the server.
3. Upon receipt of a message, the client signs it, labeling the result as an 'attestation', and returns it to the server.
4. The client concludes upon losing connection with the server.



## License

This project is licensed under the BSD 3-Clause License. Refer to [LICENSE](LICENSE) for specifics.


