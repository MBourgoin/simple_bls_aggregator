//  (c) 2024 Mathias Bourgoin <mathias.bourgoin@gmail.com>
// SPDX-License-Identifier: BSD-3-Clause

use crate::crypto::{PublicKey, Signature};
use crate::p2p::{broadcast, send_message};
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, PartialEq, Debug, Clone)]
pub enum Message {
    Attestation {
        signature: Signature,
        #[serde(with = "serde_bytes")]
        state: Vec<u8>,
    },
    Block {
        level: i32,
        signature: Signature,
        #[serde(with = "serde_bytes")]
        state: Vec<u8>,
    },
    Greeting(Vec<PublicKey>),
}

impl Message {
    pub fn broadcast(
        &self,
        handler: &message_io::node::NodeHandler<Self>,
        peers: Vec<(message_io::network::Endpoint, Vec<PublicKey>)>,
    ) {
        let s = bincode::serialize(&self);
        assert!(s.is_ok(), "Failed to serialize message");
        let s = s.unwrap();
        broadcast(handler, peers, s);
    }

    pub fn send(
        &self,
        handler: &message_io::node::NodeHandler<Self>,
        target: message_io::network::Endpoint,
    ) {
        let s = bincode::serialize(&self);
        assert!(s.is_ok(), "Failed to serialize message");
        let s = s.unwrap();
        send_message(handler, target, s);
    }
}

#[cfg(test)]
mod tests {
    use crate::crypto;

    use super::*;
    const GENESIS_STATE: &[u8] = b"Genesis";
    #[test]
    fn test_block() {
        let (sk, _pk) = crypto::gen_keys();
        let signature = sk.sign(GENESIS_STATE);
        let message = Message::Block {
            level: 0,
            state: GENESIS_STATE.to_vec(),
            signature: signature.clone(),
        };
        println!("Message: {:?}", message);
        let bin = bincode::serialize(&message);
        assert!(bin.is_ok(), "Failed to serialize message");
        let bin = bin.unwrap();
        let msg = bincode::deserialize::<Message>(&bin);
        assert!(msg.is_ok(), "Failed to deserialize message");
        let msg = msg.unwrap();
        println!("Message: {:?}", msg);
        assert_eq!(msg, message);
    }

    #[test]
    fn test_greeting() {
        let (_sk, pk) = crypto::gen_keys();
        let message = Message::Greeting(vec![pk]);
        println!("Message: {:?}", message);
        let bin = bincode::serialize(&message);
        assert!(bin.is_ok());
        let bin = bin.unwrap();
        let msg = bincode::deserialize::<Message>(&bin);
        assert!(msg.is_ok());
        let msg = msg.unwrap();
        println!("Message: {:?}", msg);
        assert_eq!(msg, message);
    }

    #[test]
    fn test_attestation() {
        let (sk, _pk) = crypto::gen_keys();
        let signature = sk.sign(GENESIS_STATE);
        let message = Message::Attestation {
            state: GENESIS_STATE.to_vec(),
            signature: signature.clone(),
        };
        println!("Message: {:?}", message);
        let bin = bincode::serialize(&message);
        assert!(bin.is_ok(), "Failed to serialize message");
        let bin = bin.unwrap();
        let msg = bincode::deserialize::<Message>(&bin);
        assert!(msg.is_ok(), "Failed to deserialize message");
        let msg = msg.unwrap();
        println!("Message: {:?}", msg);
        assert_eq!(msg, message);
    }
}
