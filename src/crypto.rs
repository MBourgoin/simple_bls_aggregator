//  (c) 2024 Mathias Bourgoin <mathias.bourgoin@gmail.com>
// SPDX-License-Identifier: BSD-3-Clause

pub use blst::min_pk::PublicKey;
use blst::min_pk::{AggregateSignature as A, SecretKey as S, Signature as Sig};
use blst::BLST_ERROR;
use rand::RngCore;
use serde::{Deserialize, Serialize};

/*
- Basic   : requiring all messages signed by an aggregate signature to be distinct
const DST: &[u8] = b"BLS_SIG_BLS12381G2_XMD:SHA-256_SSWU_RO_NUL_";
- Proof of Possession:  uses a separate public key validation
   step, called a proof of possession, to defend against rogue key
   attacks.  This enables an optimization to aggregate signature
   verification for the case that all signatures are on the same
   message
const DST: &[u8] = b"BLS_SIG_BLS12381G2_XMD:SHA-256_SSWU_RO_POP_";
- Message Augmentation: signatures are generated over the concatenation of the public
 key and the message, ensuring that messages signed by different public keys are distinct
const DST: &[u8] = b"BLS_SIG_BLS12381G2_XMD:SHA-256_SSWU_RO_AUG_";
*/
// We want to every  client to sign the same message (sent by the server) with their
// own private key and then aggregate all the signatures to verify the message
// We will use the Message Augmentation for this purpose
const DST: &[u8] = b"BLS_SIG_BLS12381G2_XMD:SHA-256_SSWU_RO_AUG_";

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Signature {
    sig: Sig,
}

impl Signature {
    pub fn verify(&self, message: &[u8], public_key: &PublicKey) -> BLST_ERROR {
        self.sig.verify(false, message, DST, &[], public_key, true)
    }

    pub fn to_signature(&self) -> Sig {
        self.sig
    }

    pub fn from_bytes(bytes: &[u8]) -> Result<Self, BLST_ERROR> {
        let sig = Sig::from_bytes(bytes)?;
        Ok(Self { sig })
    }
}

#[derive(Debug, Clone)]
pub struct AggregateSignature {
    sig: A,
}

impl AggregateSignature {
    pub fn aggregate(signatures: &[&Signature]) -> Result<Self, BLST_ERROR> {
        let sigs: Vec<_> = signatures.iter().map(|s| &s.sig).collect();
        let sig = A::aggregate(&sigs, true)?;
        Ok(Self { sig })
    }

    pub fn aggregate_some(signatures: &[Option<Signature>]) -> Result<Self, BLST_ERROR> {
        let sigs: Vec<_> = signatures
            .iter()
            .filter_map(|s| s.as_ref())
            .map(|s| &s.sig)
            .collect();
        let sig = A::aggregate(&sigs, true)?;
        Ok(Self { sig })
    }

    pub fn to_signature(&self) -> Sig {
        self.sig.to_signature()
    }
    pub fn fast_aggregate_verify(&self, message: &[u8], public_keys: &[&PublicKey]) -> BLST_ERROR {
        self.to_signature()
            .fast_aggregate_verify(false, message, DST, public_keys)
    }

    pub fn from_bytes(bytes: &[u8]) -> Result<Self, BLST_ERROR> {
        let sig = A::from_signature(&Signature::from_bytes(bytes)?.sig);
        Ok(Self { sig })
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        self.sig.to_signature().to_bytes().to_vec()
    }
}
#[derive(Debug, Clone)]
pub struct SecretKey {
    sk: S,
}

impl SecretKey {
    pub fn key_gen() -> Self {
        let mut rng = rand::thread_rng();
        let mut ikm = [0u8; 32];
        rng.fill_bytes(&mut ikm);

        let sk = S::key_gen(&ikm, &[]);
        assert!(sk.is_ok(), "Failed to generate secret key");
        let sk = sk.unwrap();

        Self { sk }
    }

    pub fn sign(&self, message: &[u8]) -> Signature {
        Signature {
            sig: self.sk.sign(message, DST, &[]),
        }
    }

    pub fn sk_to_pk(&self) -> PublicKey {
        self.sk.sk_to_pk()
    }
}

pub fn gen_keys() -> (SecretKey, PublicKey) {
    let sk = SecretKey::key_gen();
    let pk = sk.sk_to_pk();
    (sk, pk)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sign_verify() {
        let sk = SecretKey::key_gen();
        let pk = sk.sk_to_pk();
        let message = b"Hello, World!";
        let signature = sk.sign(message);
        let error = signature.verify(message, &pk);
        assert_eq!(error, BLST_ERROR::BLST_SUCCESS);
    }

    #[test]
    fn test_fast_aggregate_verify() {
        let (sk1, pk1) = gen_keys();
        let (sk2, pk2) = gen_keys();
        let message = b"Hello, World!";
        let signature1 = sk1.sign(message);
        let signature2 = sk2.sign(message);
        let aggregate = AggregateSignature::aggregate(&[&signature1, &signature2]).unwrap();
        let error = aggregate.fast_aggregate_verify(message, &[&pk1, &pk2]);
        assert_eq!(error, BLST_ERROR::BLST_SUCCESS);
        let error = aggregate.fast_aggregate_verify(message, &[&pk2, &pk1]);
        assert_eq!(error, BLST_ERROR::BLST_SUCCESS);
    }

    #[test]
    fn test_wrong_public_key() {
        let (sk1, pk1) = gen_keys();
        let (_sk2, pk2) = gen_keys();
        let message = b"Hello, World!";
        let signature = sk1.sign(message);
        let error = signature.verify(message, &pk1);
        assert_eq!(error, BLST_ERROR::BLST_SUCCESS);
        let error = signature.verify(message, &pk2);
        assert_eq!(error, BLST_ERROR::BLST_VERIFY_FAIL);
    }

    #[test]
    fn test_fast_aggregate_verify_wrong_public_key() {
        let (sk1, pk1) = gen_keys();
        let (sk2, _pk2) = gen_keys();
        let (_sk3, pk3) = gen_keys();
        let message = b"Hello, World!";
        let signature1 = sk1.sign(message);
        let signature2 = sk2.sign(message);
        let aggregate = AggregateSignature::aggregate(&[&signature1, &signature2]).unwrap();
        let error = aggregate.fast_aggregate_verify(message, &[&pk1, &pk3]);
        assert_eq!(error, BLST_ERROR::BLST_VERIFY_FAIL);
    }
}
