//  (c) 2024 Mathias Bourgoin <mathias.bourgoin@gmail.com>
// SPDX-License-Identifier: BSD-3-Clause

use crate::{
    crypto::{PublicKey, SecretKey},
    message::Message,
};

const GENESIS_STATE: &str = "Genesis";
// Forge a block
pub fn block(sk: &SecretKey, state: Option<&[u8]>, current_level: i32) -> Message {
    let state = state.unwrap_or_else(|| GENESIS_STATE.as_bytes());
    let signature = sk.sign(state);
    Message::Block {
        level: current_level,
        state: state.to_vec(),
        signature,
    }
}

// Forge a genesis block
pub fn genesis(sk: &SecretKey, current_level: i32) -> Message {
    block(sk, None, current_level)
}

// Forge a greeting message
pub fn greeting(pks: Vec<PublicKey>) -> Message {
    Message::Greeting(pks)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::crypto::gen_keys;

    #[test]
    fn forge_genesis() -> Result<(), Box<dyn std::error::Error>> {
        let (sk, _pk) = gen_keys();
        let current_level = 0;
        let genesis = genesis(&sk, current_level);
        let genesis = bincode::serialize(&genesis);
        assert!(genesis.is_ok(), "Failed to forge genesis block");
        let genesis = genesis.unwrap();
        let message: Message = bincode::deserialize(&genesis)?;
        match message {
            Message::Block {
                level,
                state,
                signature,
            } => {
                assert_eq!(level, current_level, "Invalid level");
                assert_eq!(state, "Genesis".as_bytes(), "Invalid state hash");
                assert_eq!(
                    signature,
                    sk.sign("Genesis".as_bytes()),
                    "Invalid signature"
                );
            }
            _ => panic!("Invalid message kind"),
        }
        Ok(())
    }

    #[test]
    fn forge_block() -> Result<(), Box<dyn std::error::Error>> {
        let (sk, _pk) = gen_keys();
        let current_level = 1;
        let state = "State".as_bytes();
        let block = block(&sk, Some(state), current_level);
        let block = bincode::serialize(&block);
        assert!(block.is_ok(), "Failed to forge block");
        let block = block.unwrap();
        let message: Message = bincode::deserialize(&block)?;
        match message {
            Message::Block {
                level,
                state: sh,
                signature,
            } => {
                assert_eq!(level, current_level, "Invalid level");
                assert_eq!(sh, state, "Invalid state hash");
                assert_eq!(signature, sk.sign(state), "Invalid signature");
            }
            _ => panic!("Invalid message kind"),
        }
        Ok(())
    }

    #[test]
    fn forge_greeting() -> Result<(), Box<dyn std::error::Error>> {
        let (_sk, pk) = gen_keys();
        let pks = vec![pk];
        let greeting = greeting(pks);
        let greeting = bincode::serialize(&greeting);
        assert!(greeting.is_ok(), "Failed to forge greeting");
        let greeting = greeting.unwrap();
        let message: Message = bincode::deserialize(&greeting)?;
        match message {
            Message::Greeting(pks) => {
                assert_eq!(pks.len(), 1, "Invalid number of public keys");
                assert_eq!(pks[0], pk, "Invalid public key");
            }
            _ => panic!("Invalid message kind"),
        }
        Ok(())
    }
}
