//  (c) 2024 Mathias Bourgoin <mathias.bourgoin@gmail.com>
// SPDX-License-Identifier: BSD-3-Clause

use crate::crypto::PublicKey;
use log::{debug, warn};
use message_io::{
    network::{Endpoint, SendStatus},
    node::NodeHandler,
};

pub fn broadcast<T>(
    handler: &NodeHandler<T>,
    peers: Vec<(Endpoint, Vec<PublicKey>)>,
    message: Vec<u8>,
) {
    peers
        .iter()
        .for_each(|peer| match handler.network().send(peer.0, &message) {
            SendStatus::Sent => debug!("Sent message {:?} to peer {}", &message, peer.0),
            e => warn!(
                "Error sending message {:?} to peer {}: {:?}",
                message, peer.0, e
            ),
        });
}

pub fn send_message<T>(handler: &NodeHandler<T>, target: Endpoint, message: Vec<u8>) {
    match handler.network().send(target, &message) {
        SendStatus::Sent => debug!("Sent message {:?} to target {:?}", &message, target),
        e => warn!(
            "Error sending message {:?} to endpoint {:?}: {:?}",
            message, target, e
        ),
    }
}
