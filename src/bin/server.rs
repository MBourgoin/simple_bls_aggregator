//  (c) 2024 Mathias Bourgoin <mathias.bourgoin@gmail.com>
// SPDX-License-Identifier: BSD-3-Clause

use message_io::network::{NetEvent, Transport};
use message_io::node::{self};

use log::{debug, info};
use std::collections::HashMap;
use utils::{
    cli,
    crypto::{gen_keys, AggregateSignature, PublicKey, Signature},
    forge,
    message::Message,
};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let (max_level, connections) = cli::read_args();

    let mut current_level = 0;
    let mut peerid = 0;
    let mut id_of_endpoints = HashMap::new();

    let (sk, pk) = gen_keys();

    let mut client_pks: Vec<PublicKey> = Vec::new();
    let mut attestations: Vec<Option<Signature>> = vec![None; connections as usize];
    let mut received_attestations = 0;
    debug!("Secret key: {:?}", sk);
    debug!("Public key: {:?}", pk);

    let mut peers = Vec::new();
    let (handler, listener) = node::split::<Message>();

    handler
        .network()
        .listen(Transport::FramedTcp, "127.0.0.1:7965")
        .unwrap();

    listener.for_each(move |event| match event.network() {
        NetEvent::Connected(_, _) => debug!("Connected"),
        NetEvent::Accepted(endpoint, _listener) => {
            info!("Client connected");
            id_of_endpoints.insert(endpoint, peerid);
            peerid += 1;
        }
        NetEvent::Message(endpoint, data) => {
            debug!("Received message 'from: {:?}", endpoint);
            let message: Result<Message, _> = bincode::deserialize(data);
            assert!(message.is_ok(), "Failed to deserialize message");
            let message = message.unwrap();
            match message {
                Message::Greeting(cpk) => {
                    debug!("Received greeting from peer {}", endpoint);
                    peers.push((endpoint, cpk.clone()));
                    client_pks.push(cpk[0]);
                    if peers.len() == connections as usize {
                        info!("All peers connected");
                        let all_pks: Vec<PublicKey> =
                            vec![pk].into_iter().chain(client_pks.clone()).collect();
                        let message = forge::greeting(all_pks.clone());
                        message.broadcast(&handler, peers.clone());

                        info!("Broadcast Genesis block");
                        let message = forge::genesis(&sk, current_level);
                        message.broadcast(&handler, peers.clone());
                    }
                }
                Message::Attestation {
                    signature,
                    state: _,
                } => {
                    info!("Received attestation  from peer {}", endpoint);
                    debug!("Attestation = {:?}", signature);
                    attestations[id_of_endpoints[&endpoint] as usize] = Some(signature);
                    received_attestations += 1;

                    if received_attestations == connections {
                        let sig = AggregateSignature::aggregate_some(&attestations);
                        assert!(sig.is_ok(), "Failed to aggregate signatures");
                        let sig = sig.unwrap();

                        let state = sig.to_bytes();
                        current_level += 1;
                        let block = forge::block(&sk, Some(state.as_ref()), current_level);

                        attestations = attestations.iter().map(|_| None).collect();
                        received_attestations = 0;

                        info!("Broadcast block for level {}", current_level);
                        block.broadcast(&handler, peers.clone());
                        if current_level == max_level {
                            info!("Max level {} reached", max_level);
                            handler.stop();
                        }
                    }
                }

                Message::Block { .. } => {
                    debug!("Received block from peer {}", endpoint);
                }
            }
        }
        NetEvent::Disconnected(_endpoint) => info!("Client disconnected"), //Tcp or Ws
    });

    Ok(())
}
