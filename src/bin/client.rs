//  (c) 2024 Mathias Bourgoin <mathias.bourgoin@gmail.com>
// SPDX-License-Identifier: BSD-3-Clause

use log::{debug, error, info};
use message_io::network::{NetEvent, Transport};
use message_io::node::{self, NodeEvent};
use std::process::exit;

use utils::{
    crypto::{self, AggregateSignature, PublicKey},
    message::Message,
};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();

    let (sk, pk) = crypto::gen_keys();
    debug!("Secret key: {:?}", sk);
    debug!("Public key: {:?}", pk);
    let mut server_pk = None;
    let mut all_client_pks: Vec<PublicKey> = Vec::new();

    let remote_addr = "127.0.0.1:7965";
    let (handler, listener) = node::split::<Message>();
    let (server_id, local_addr) = handler
        .network()
        .connect(Transport::FramedTcp, remote_addr)
        .unwrap();
    let mut predecessor: Option<Vec<u8>> = None;
    listener.for_each(move |event| match event {
        NodeEvent::Network(net_event) => match net_event {
            NetEvent::Connected(_, established) => {
                if established {
                    info!(
                        "Connected to server at {} by {}",
                        server_id.addr(),
                        Transport::FramedTcp
                    );
                    debug!("Client identified by local port: {}", local_addr.port());
                    handler.signals().send(Message::Greeting(vec![pk]));
                } else {
                    debug!(
                        "Can not connect to server at {} by {}",
                        remote_addr,
                        Transport::FramedTcp
                    )
                }
            }
            NetEvent::Accepted(_, _) => unreachable!(), // Only generated when a listener accepts
            NetEvent::Message(_, data) => {
                let message: Message = bincode::deserialize(data).unwrap();
                debug!("Received message: {:?}", message);
                match message {
                    Message::Block {
                        level,
                        state,
                        signature,
                    } => {
                        info!("Received block from server");
                        if let Some(spk) = server_pk {
                            let error = signature.verify(&state, &spk);
                            if error != blst::BLST_ERROR::BLST_SUCCESS {
                                error!("Client signature mismatch");
                                return;
                            }
                            if state.as_slice() == b"Genesis" {
                                info!("Genesis block verified");
                            } else if let Some(predecessor) = predecessor.clone() {
                                debug!("Check own signature is present in aggregate signature");
                                let sig = AggregateSignature::from_bytes(&state);
                                assert!(sig.is_ok(), "Failed to deserialize signature");
                                let sig = sig.unwrap();
                                let error = sig.fast_aggregate_verify(
                                    &predecessor,
                                    &all_client_pks.iter().collect::<Vec<_>>()[..],
                                );
                                assert_eq!(error, blst::BLST_ERROR::BLST_SUCCESS);
                            } else {
                                panic!("No predecessor block");
                            }
                        } else {
                            error!("Server signature mismatch");
                            return;
                        };
                        predecessor = Some(state.to_vec());

                        let signature = sk.sign(&state);
                        debug!("Signature: {:?}", signature);
                        info!("Sending attestation to server for level {}", level);
                        handler.signals().send(Message::Attestation {
                            state: state.clone(),
                            signature,
                        });
                    }
                    Message::Attestation {
                        state: _,
                        signature: _,
                    } => {
                        debug!("Received attestation from server");
                    }

                    Message::Greeting(pk) => {
                        debug!("Received greeting from server");
                        match pk.split_first() {
                            None => {
                                error!("No public keys in greeting");
                            }
                            Some((spk, client_pks)) => {
                                server_pk = Some(*spk);
                                all_client_pks = client_pks.to_vec();
                            }
                        }
                    }
                }
            }
            NetEvent::Disconnected(_) => {
                debug!("Disconnected from server at {}", server_id.addr());
                exit(0);
            }
        },
        NodeEvent::Signal(message) => {
            // might fail if server is not connected (for instance at end of
            // bounded test)
            message.send(&handler, server_id);
        }
    });
    Ok(())
}
