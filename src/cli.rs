//  (c) 2024 Mathias Bourgoin <mathias.bourgoin@gmail.com>
// SPDX-License-Identifier: BSD-3-Clause

use std::process::exit;

/* Very simple cli to choose number of expected clients and number of levels to reach before closing the server */
pub fn read_args() -> (i32, u32) {
    let mut max_level = -1;
    let mut connections = 2;
    let args: Vec<String> = std::env::args().collect();

    let mut i = 1;
    while i < args.len() {
        match args[i].as_str() {
            "--max-level" => {
                max_level = args[i + 1].parse::<u32>().unwrap() as i32;
                i += 2;
            }
            "--connections" => {
                connections = args[i + 1].parse::<u32>().unwrap();
                i += 2;
            }
            "-h" => {
                println!("Usage: server [-max_level <int>] [-connections <int>] [-h]");
                exit(1);
            }
            _ => {
                println!("Invalid option: {}", args[i]);
                exit(2);
            }
        }
    }
    (max_level, connections)
}
